# Exercise 8
## 1. Reset commit
Sau khi clone, cần tìm commit code để reset
```bash
git log --oneline
```
Sau khi có commit code, reset soft để trở về lúc commit và push lên
```bash
git reset --soft 138fbca
git push -f
```